//
//  GameScene.swift
//  P08-pradhan
//
//  Created by Nimesh on 5/9/17.
//  Copyright © 2017 npradha2. All rights reserved.
//

import SpriteKit
import GameplayKit


class GameScene: SKScene, SKPhysicsContactDelegate {
    
    var square = SKSpriteNode()
    var wall = SKSpriteNode()
    var destination = SKSpriteNode()
    var enemy = SKSpriteNode()
    var key = SKSpriteNode()
    var diamond1 = SKSpriteNode()
    var diamond2 = SKSpriteNode()
    var diamond3 = SKSpriteNode()
    var diamond4 = SKSpriteNode()
    var levelCompleteNode = SKShapeNode()
    
    var scoreLabel = SKLabelNode()
    var restartButton = SKSpriteNode()
    
    var walkRightAction = SKAction()
    var walkLeftAction = SKAction()
    var holdBarAction = SKAction()
    
    var doorUnlocked = Bool()
    var gameOver = Bool()
    
    var score = Int()

    struct PhyCat {
        static let Square : UInt32 = 0x1 << 1
        static let Wall : UInt32 = 0x1 << 2
        static let Enemy : UInt32 = 0x1 << 3
        static let Key : UInt32 = 0x1 << 4
        static let Destination : UInt32 = 0x1 << 5
        static let Diamond1 : UInt32 = 0x1 << 6
        static let Diamond2 : UInt32 = 0x1 << 7
        static let Diamond3 : UInt32 = 0x1 << 8
        static let Diamond4 : UInt32 = 0x1 << 9
    }
    
    override func didMove(to view: SKView) {
        self.startGame()
    }
    
    func startGame(){
        
      //  self.gameOverLabel()
        score = 0
        doorUnlocked = false
        gameOver = false
      //  print(UIFont.familyNames)
        self.physicsWorld.contactDelegate = self
        self.createLevel1()
        self.createScoreLabel()
        
        let swipeRightRec = UISwipeGestureRecognizer()
        let swipeLeftRec = UISwipeGestureRecognizer()
        let swipeUpRec = UISwipeGestureRecognizer()
        let swipeDownRec = UISwipeGestureRecognizer()
        
        swipeRightRec.addTarget(self, action: #selector(GameScene.swipedRight) )
        swipeRightRec.direction = .right
        self.view!.addGestureRecognizer(swipeRightRec)
        
        swipeLeftRec.addTarget(self, action: #selector(GameScene.swipedLeft) )
        swipeLeftRec.direction = .left
        self.view!.addGestureRecognizer(swipeLeftRec)
        
        swipeUpRec.addTarget(self, action: #selector(GameScene.swipedUp) )
        swipeUpRec.direction = .up
        self.view!.addGestureRecognizer(swipeUpRec)
        
        swipeDownRec.addTarget(self, action: #selector(GameScene.swipedDown) )
        swipeDownRec.direction = .down
        self.view!.addGestureRecognizer(swipeDownRec)
    }
    
    func createSquare(){
        square = SKSpriteNode(imageNamed: "square")
        square.position = CGPoint(x: 30, y: 60 * 3 + 30)
        square.zPosition = 11
        square.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 60, height: 60))
        square.physicsBody?.affectedByGravity = false
        square.physicsBody?.categoryBitMask = PhyCat.Square
        square.physicsBody?.collisionBitMask = PhyCat.Wall
        square.physicsBody?.contactTestBitMask = PhyCat.Enemy | PhyCat.Wall | PhyCat.Key | PhyCat.Destination | PhyCat.Diamond1 | PhyCat.Diamond2 | PhyCat.Diamond3 | PhyCat.Diamond4
        square.physicsBody?.isDynamic = true
        square.physicsBody?.allowsRotation = false
        self.addChild(square)
    }
    
    func createWall(x: Int, y:Int){
        let X = x + 30
        let Y = y + 30
        wall = SKSpriteNode(imageNamed: "wall")
        wall.position = CGPoint(x: X, y: Y)
        wall.zPosition = 9
        wall.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 60, height: 60))
        wall.physicsBody?.affectedByGravity = false
        wall.physicsBody?.categoryBitMask = PhyCat.Wall
        wall.physicsBody?.collisionBitMask = PhyCat.Square
        wall.physicsBody?.contactTestBitMask = PhyCat.Square
        wall.physicsBody?.isDynamic = false
        wall.physicsBody?.allowsRotation = false
        self.addChild(wall)
    }

    func createKey(x: Int, y:Int){
        let X = x * 60 + 30
        let Y = y * 60 + 30
        key = SKSpriteNode(imageNamed: "key")
        key.position = CGPoint(x: X, y: Y)
        key.zPosition = 10
        key.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 57, height: 57))
        key.physicsBody?.affectedByGravity = false
        key.physicsBody?.categoryBitMask = PhyCat.Key
        key.physicsBody?.collisionBitMask = 0
        key.physicsBody?.contactTestBitMask = PhyCat.Square
        key.physicsBody?.isDynamic = false
        key.physicsBody?.allowsRotation = false
        self.addChild(key)
    }
    
    func createRod(x: Int, y:Int){
       // print("creating rod")
        let X = x * 60 + 30
        let Y = y * 60 + 30
        let rod = SKSpriteNode(imageNamed: "rod")
        rod.position = CGPoint(x: X, y: Y)
        rod.zPosition = 7
        self.addChild(rod)
    }

    func createDestination(x: Int, y:Int){
        let X = x * 60 + 30
        let Y = y * 60 + 30
        destination = SKSpriteNode(imageNamed: "door")
        destination.position = CGPoint(x: X, y: Y)
        destination.zPosition = 9
        self.addChild(destination)
    }
    
    func createEnemy(x: Int, y:Int) -> SKSpriteNode{
        let X = x * 60 + 30
        let Y = y * 60 + 30
        self.enemy = SKSpriteNode(imageNamed: "enemy")
        self.enemy.position = CGPoint(x: X, y: Y)
        self.enemy.zPosition = 10
        self.enemy.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 57, height: 57))
        self.enemy.physicsBody?.affectedByGravity = false
        self.enemy.physicsBody?.categoryBitMask = PhyCat.Enemy
        self.enemy.physicsBody?.collisionBitMask = 0
        self.enemy.physicsBody?.contactTestBitMask = PhyCat.Square
        self.enemy.physicsBody?.isDynamic = true
        self.enemy.physicsBody?.allowsRotation = false
        return self.enemy
    }
    
    func createDiamond(x: Int, y:Int, d:Int) -> SKSpriteNode {
        let X = x * 60 + 30
        let Y = y * 60 + 30
        let diamond = SKSpriteNode(imageNamed: "diamond")
        diamond.position = CGPoint(x: X, y: Y)
        diamond.zPosition = 10
        diamond.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 57, height: 57))
        diamond.physicsBody?.affectedByGravity = false
        if(d == 1){
            diamond.physicsBody?.categoryBitMask = PhyCat.Diamond1
        }
        if(d == 2){
            diamond.physicsBody?.categoryBitMask = PhyCat.Diamond2
        }
        if(d == 3){
            diamond.physicsBody?.categoryBitMask = PhyCat.Diamond3
        }
        if(d == 4){
            diamond.physicsBody?.categoryBitMask = PhyCat.Diamond4
        }
        diamond.physicsBody?.collisionBitMask = 0
        diamond.physicsBody?.contactTestBitMask = PhyCat.Square
        diamond.physicsBody?.isDynamic = true
        diamond.physicsBody?.allowsRotation = false
        diamond.run(SKAction.repeatForever(SKAction.sequence([SKAction.moveBy(x: 0, y: 8, duration: 1.0), SKAction.moveBy(x: 0, y: -16, duration: 2.0), SKAction.moveBy(x: 0, y: 8, duration: 1.0)])))
        return diamond
    }
    
    func unlockDoor(){
        let location = destination.position
        destination.removeFromParent()
        destination = SKSpriteNode(imageNamed: "destination")
        destination.position = location
        destination.zPosition = 10
        destination.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 57, height: 57))
        destination.physicsBody?.affectedByGravity = false
        destination.physicsBody?.categoryBitMask = PhyCat.Destination
        destination.physicsBody?.collisionBitMask = 0
        destination.physicsBody?.contactTestBitMask = PhyCat.Square
        destination.physicsBody?.isDynamic = false
        destination.physicsBody?.allowsRotation = false
        self.addChild(destination)
        doorUnlocked = true
    }
    
    func createScoreLabel()
    {
        scoreLabel.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2 + self.frame.height/2.5)
        scoreLabel.text = "Points :\(score)"
        scoreLabel.fontName = "Arial Rounded MT Bold"
        scoreLabel.fontColor = SKColor.white
        scoreLabel.zPosition = 20
        scoreLabel.fontSize = 40
        self.addChild(scoreLabel)
    }
    
    func updateScore(points: Int){
        score += points
        scoreLabel.text = "Points :\(score)"
    }
    
    func gameOverLabel()
    {
        //var levelCompleteNode = SKShapeNode()
        levelCompleteNode = SKShapeNode(rect: CGRect(x: self.size.width / 2 - 250, y: self.size.height / 2 - 30, width: 500, height: 80))
        levelCompleteNode.fillColor = UIColor.blue
        levelCompleteNode.zPosition = 19
        let gameOverLabel = SKLabelNode()
        gameOverLabel.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2)
        gameOverLabel.text = "Well Done! You Score: \(score)"
        gameOverLabel.fontName = "Arial Rounded MT Bold"
        gameOverLabel.fontColor = SKColor.white
        gameOverLabel.zPosition = 20
        gameOverLabel.fontSize = 40
        levelCompleteNode.addChild(gameOverLabel)
        gameOverLabel.color = SKColor.cyan
        self.addChild(levelCompleteNode)
    }
    
    func createRestartButton(){
        restartButton = SKSpriteNode(imageNamed : "restart")
        restartButton.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
        restartButton.zPosition = 20
        restartButton.setScale(0)
        self.addChild(restartButton)
        restartButton.run(SKAction.scale(to: 0.5, duration: 0.3))
    }
    
    func createLevel1(){
        
        self.createSquare()
        self.createDestination(x: 6, y: 4)
        self.createKey(x: 10, y: 3)
        
        var upperbound = Int(self.size.width / 60) + 1
        for i in 0...upperbound{
            self.createWall(x:60 * i, y:Int(60 * 6))
            self.createRod(x: i, y: 0)
            if(i == 5){
                continue
            }
            self.createWall(x:60 * i, y:0)
        }
        
        upperbound = Int(self.size.height / 60) + 1
        for i in 0...upperbound{
            self.createWall(x:Int(60 * 11), y:60 * i)
            if(i == 2 || i == 3 || i == 4){
                continue
            }
            self.createWall(x:0, y:60 * i)
            
        }
        
        for i in 3...5{
            self.createWall(x: 60 * i, y: 60 * 4)
        }
        for i in 7...11{
            self.createWall(x: 60 * i, y: 60 * 4)
        }
        for i in 1...2{
            self.createWall(x: 60 * 2, y: 60 * i)
        }
        for i in 2...3{
            self.createWall(x: 60 * 5, y: 60 * i)
        }
        for i in 8...11{
            self.createWall(x: 60 * i, y: 60 * 2)
        }
        
        diamond1 = createDiamond(x: 7, y: 5, d: 1)
        self.addChild(diamond1)
        diamond2 = createDiamond(x: 4, y: 2, d: 2)
        self.addChild(diamond2)

        let enemy1 = self.createEnemy(x: 5, y: 5)
        self.addChild(enemy1)
        
        enemy1.run(SKAction.repeatForever(SKAction.sequence([SKAction.moveBy(x: 60 * 5, y: 0, duration: 5),SKAction.moveBy(x: -60 * 5, y: 0, duration: 5)])))

        let enemy2 = self.createEnemy(x: 4, y: 1)
        self.addChild(enemy2)
        enemy2.run(SKAction.repeatForever(SKAction.sequence([SKAction.moveBy(x: 0, y: 60 * 2, duration: 3),SKAction.moveBy(x: 0, y: -60 * 2, duration: 3)])))
        
        let enemy3 = self.createEnemy(x: 5, y: 1)
        self.addChild(enemy3)
        enemy3.run(SKAction.repeatForever(SKAction.sequence([SKAction.moveBy(x: 60 * 5, y: 0, duration: 2),SKAction.moveBy(x: -60 * 5, y: 0, duration: 2)])))

        let enemy4 = self.createEnemy(x: 1, y: 1)
        self.addChild(enemy4)
        enemy4.run(SKAction.repeatForever(SKAction.sequence([SKAction.moveBy(x: 0, y: 60 * 2, duration: 4),SKAction.moveBy(x: 0, y: -60 * 2, duration: 4)])))

    }
    
    func swipedRight(sender:UISwipeGestureRecognizer){
        self.playerwalkRight()
        square.run(walkRightAction)
        square.run(SKAction.moveBy(x: 60, y: 0, duration: 0.2))
        //print("swiped right")
    }
    
    func swipedLeft(sender:UISwipeGestureRecognizer){
        self.playerwalkLeft()
        square.run(walkLeftAction)
        square.run(SKAction.moveBy(x: -60, y: 0, duration: 0.2))
        //print("swiped left")
    }
    
    func swipedUp(sender:UISwipeGestureRecognizer){
        self.playerholdBar()
        square.run(holdBarAction)
        square.run(SKAction.moveBy(x: 0, y: 60, duration: 0.2))
        //print("swiped up")
    }
    
    func swipedDown(sender:UISwipeGestureRecognizer){
        self.playerholdBar()
        square.run(holdBarAction)
        square.run(SKAction.moveBy(x: 0, y: -60, duration: 0.2))
        //print("swiped down")
    }
    
    func playerwalkRight()
    {
        var walkRight = SKTextureAtlas()
        var walkRightArray = [SKTexture]()
        
        walkRight = SKTextureAtlas(named: "walkright.atlas")
        for i in 1...walkRight.textureNames.count
        {
            let Name = "walkright\(i).png"
            walkRightArray.append(SKTexture(imageNamed: Name))
        }
        walkRightAction = SKAction.animate(with: walkRightArray, timePerFrame: 0.5 / 6)
    }
    
    func playerwalkLeft()
    {
        var walkLeft = SKTextureAtlas()
        var walkLeftArray = [SKTexture]()
        
        walkLeft = SKTextureAtlas(named: "walkleft.atlas")
        for i in 1...walkLeft.textureNames.count
        {
            let Name = "walkleft\(i).png"
            walkLeftArray.append(SKTexture(imageNamed: Name))
        }
        walkLeftAction = SKAction.animate(with: walkLeftArray, timePerFrame: 0.5 / 6)
    }
    
    func playerholdBar()
    {
        let f0 = SKTexture.init(imageNamed: "holdbar")
        let frames: [SKTexture] = [f0]
        holdBarAction = SKAction.animate(with: frames, timePerFrame: 0.5)
    }
    
    func didBegin(_ contact: SKPhysicsContact)
    {
        
        let firstBody = contact.bodyA
        let secondBody = contact.bodyB
        if ((firstBody.categoryBitMask == PhyCat.Square && secondBody.categoryBitMask == PhyCat.Enemy)||(firstBody.categoryBitMask == PhyCat.Enemy && secondBody.categoryBitMask == PhyCat.Square)){
            self.removeAllActions()
            self.createRestartButton()
            gameOver = true
            print("game over")
        }
        
        if ((firstBody.categoryBitMask == PhyCat.Square && secondBody.categoryBitMask == PhyCat.Key)||(firstBody.categoryBitMask == PhyCat.Key && secondBody.categoryBitMask == PhyCat.Square)){
            print("door unlocked")
            self.updateScore(points: 200)
            key.removeFromParent()
            self.unlockDoor()
        }
        
        if ((firstBody.categoryBitMask == PhyCat.Square && secondBody.categoryBitMask == PhyCat.Diamond1)||(firstBody.categoryBitMask == PhyCat.Diamond1 && secondBody.categoryBitMask == PhyCat.Square)){
            print("diamond collected")
            self.updateScore(points: 100)
            diamond1.removeFromParent()
        }
        
        if ((firstBody.categoryBitMask == PhyCat.Square && secondBody.categoryBitMask == PhyCat.Diamond2)||(firstBody.categoryBitMask == PhyCat.Diamond2 && secondBody.categoryBitMask == PhyCat.Square)){
            self.updateScore(points: 100)
            print("diamond collected")
            diamond2.removeFromParent()
        }
        
        if ((firstBody.categoryBitMask == PhyCat.Square && secondBody.categoryBitMask == PhyCat.Diamond3)||(firstBody.categoryBitMask == PhyCat.Diamond3 && secondBody.categoryBitMask == PhyCat.Square)){
            self.updateScore(points: 100)
            print("diamond collected")
            diamond1.removeFromParent()
        }
        
        if ((firstBody.categoryBitMask == PhyCat.Square && secondBody.categoryBitMask == PhyCat.Diamond4)||(firstBody.categoryBitMask == PhyCat.Diamond4 && secondBody.categoryBitMask == PhyCat.Square)){
            self.updateScore(points: 100)
            print("diamond collected")
            diamond4.removeFromParent()
        }
        
        if(doorUnlocked){
            if ((firstBody.categoryBitMask == PhyCat.Square && secondBody.categoryBitMask == PhyCat.Destination)||(firstBody.categoryBitMask == PhyCat.Destination && secondBody.categoryBitMask == PhyCat.Square)){
                doorUnlocked = false
                gameOver = true
                self.updateScore(points: 500)
                print("level complete")
                self.gameOverLabel()
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches{
            let location  = touch.location(in: self)
                if(gameOver && restartButton.contains(location)){
                    self.restartGame()
            }
            if(gameOver && levelCompleteNode.contains(location)){
                self.restartGame()
            }
        }
    }
    
    func restartGame(){
        self.removeAllChildren()
        self.startGame()
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
      }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
      }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
      }
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
}
